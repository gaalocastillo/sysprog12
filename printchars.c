#include "csapp.h"
typedef struct {
int *buf;
int n;
int front;
int rear;
sem_t mutex;
sem_t slots;
sem_t items;
} sbuf_t;

sbuf_t buffer;
void *writeContent(void * argv);
void *readContent(void *argv);
int readcnt;
sem_t w;
sem_t mutex;

//Funcion que escribe el contenido del archivo.
void * writeContent(void * argv)
{
	int fd;
	char *file;
	file = (char *) argv;
	int count = 0;
	char c;
	fd = Open(file, O_RDONLY, 0);
		while(buffer.n > count){
			Read(fd,&c,1);
			P(&w);
			sbuf_insert(&buffer, c);
			V(&w);
			//Muestro el caracter insertado en el buffer.
			printf("Insertando caracter...: %c\n",c);
			count = count + 1;
			//usleep recibe tiempo en microsegundos.			
			usleep(50000);
		}			
		Close(fd);

	return NULL;	
}

//Funcion que lee el contenido del archivo.
void * readContent(void *argv)
{
 	char c;
 	char *received;
 	int count = 0;
 	received = (char *)argv;
 	while(buffer.n > count){
		P(&mutex);		
		c = sbuf_remove(&buffer);
		V(&mutex);		
 		received[count] = c;
 		count = count + 1;
 		//Leo caracter a caracter que contiene el buffer.
 		printf("Lectura de buffer compartido: %c\n",c);
 		//usleep recibe tiempo en microsegundos.
 		usleep(1000000);
 	}
 	printf("Texto completo: %s\n",received);
 	return NULL;
 }

int main(int argc, char **argv)
{
	Sem_init(&mutex, 0, 1);
	Sem_init(&w, 0, 1);
	//Hilo del productor.
	pthread_t hilo1;
	//Hilo del consumidor.
	pthread_t hilo2;
	char *filename;
	struct stat fileStat;
	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(0);
	}
	filename = argv[1];
	if (stat(filename, &fileStat)<0){	
		printf("Archivo %s no fue encontrado\n",filename);
	}
	else{
		//El buffer debe de tener un tamano fijo de 10 caracteres.
		printf("Abriendo archivo %s...\n",filename);
		sbuf_init(&buffer, 10);
		char received[10];
		Pthread_create(&hilo1, NULL, writeContent, filename);
		Pthread_create(&hilo2, NULL, readContent, received);
		Pthread_join(hilo1, NULL);
		Pthread_join(hilo2, NULL);
	}
	return 0;
}

void sbuf_init(sbuf_t *sp, int n)
{
    sp->buf = Calloc(n, sizeof(int));
    sp->n = n; /* Buffer holds max of n items */
    sp->front = sp->rear = 0; /* Empty buffer iff front == rear */
    Sem_init(&sp->mutex, 0, 1); /* Binary semaphore for locking */
    Sem_init(&sp->slots, 0, n); /* Initially, buf has n empty slots */
    Sem_init(&sp->items, 0, 0); /* Initially, buf has zero data items */
}

void sbuf_deinit(sbuf_t *sp)
{
    Free(sp->buf);
}

void sbuf_insert(sbuf_t *sp, int item)
{
    P(&sp->slots); /* Wait for available slot */
    P(&sp->mutex); /* Lock the buffer */
    sp->buf[(++sp->rear)%(sp->n)] = item; /* Insert the item */
    V(&sp->mutex); /* Unlock the buffer */
    V(&sp->items); /* Announce available item */
}

int sbuf_remove(sbuf_t *sp)
{
    int item;
    P(&sp->items); /* Wait for available item */
    P(&sp->mutex); /* Lock the buffer */
    item = sp->buf[(++sp->front)%(sp->n)]; /* Remove the item */
    V(&sp->mutex); /* Unlock the buffer */
    V(&sp->slots); /* Announce available slot */
    return item;
}